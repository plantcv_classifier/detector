# Compile cocoapi
1. Go to path/to/cocoapi/PythonAPI
2. Run `python setup.py build_ext install`

# Process data
1. Create training data directory
2. Edit path/to/detector/process-data.py
    * Replace RAW_DIR with training data directory
3. Copy all images and xml (annotation) in training data directory
4. Run `python process-data.py`

# Setup detector
1. Go to path/to/detector/
2. Run `list.bat`
3. Run `python object_detection/dataset_tools/create_plant_tf_record.py --input_path=<training-data-dir> --output_path=<detector-data-dir>`
    Ex: python object_detection/dataset_tools/create_plant_tf_record.py --input_path=C:/Users/Gio/Documents/projects/data --output_path=C:/Users/Gio/Documents/projects/tensorflow-model/research/data

# Train detector
1. Go to path/to/detector/
2. Create model directory with the following contents:
    * (directory) eval
    * (directory) train
    * faster_rcnn_resnet101_plants.config
3. Edit faster_rcnn_resnet101_plants.config
    a. Replace 'C:/Users/Gio/Documents/projects/tensorflow-model/research/' with <detector-directory>
4. Run `python object_detection/train.py --logtostderr --pipeline_config_path=model/faster_rcnn_resnet101_plants.config --train_dir=model/train`
5. Stop when you want (ex: 30000 steps)

# Train classifier
1. Go to path/to/detector/object_detection/resnet50_classifier
2. Copy training images to path/to/detector/object_detection/resnet50_classifier/training_images
    * Copy images to corresponding directory name
3. Run `python object_detection/resnet50_classifier/train_resnetv2.py`

# Run predictor
1. Go to path/to/detector/object_detection
2. Copy images to classify inside path/to/detector/object_detection/test_images
    * name each image as `image{number}.jpg` ex: image1.jpg, image2.jpg
2. Run `jupyter notebook .`
3. Open and run object_detection_with_classifier.ipynb
    * Edit range for images to number of images inside path/to/detector/object_detection/test_images