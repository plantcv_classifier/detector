import os
import shutil
import xml.etree.ElementTree as ET
import re

RAW_DIR = 'C:\\Users\\Gio\\Documents\\projects\\shan\\thesis\\SSD-Tensorflow\\plant-data\\extracted'
ANN_DIR = 'C:\\Users\\Gio\\Documents\\projects\\shan\\thesis\\SSD-Tensorflow\\plant-data\\extracted\\Annotations'
IMG_DIR = 'C:\\Users\\Gio\\Documents\\projects\\shan\\thesis\\SSD-Tensorflow\\plant-data\\extracted\\JPEGImages'
file_count = 0

def change_path_value(xml_file):
    # Get root
    file_path = os.path.join(RAW_DIR, xml_file)
    tree = ET.parse(file_path)
    root = tree.getroot()
    
    # Find elements
    filename_element = root.find('filename')
    path = root.find('path')

    # Replace filename and path
    filename_element.text = re.sub('(\\.xml)|(\\.jpg)|(\\.jpeg)|(\\.png)', '', filename_element.text.lower()) + '.jpg'
    path.text = os.path.join(IMG_DIR, filename_element.text)
    
    # Create correct name
    xml_name = re.sub('(jpg)|(jpeg)', 'xml', filename_element.text.lower())
    
    # Remove old xml
    os.remove(file_path)

    # Save edited xml
    tree.write(os.path.join(ANN_DIR, xml_name))

    print('Processed:', xml_name)

def rename_to_jpg(img_file):
    new_img_name = re.sub('\\.((jpg)|(jpeg)|(png)|(JPG)|(JPEG)|(PNG))$', '', img_file) + '.jpg'
    os.rename(os.path.join(RAW_DIR, img_file), os.path.join(IMG_DIR, new_img_name))
    print('Renamed', img_file, 'to', new_img_name)

def verify(removefailed=False):
    total = 0
    correct = 0
    xml_files = os.listdir(ANN_DIR)
    for xml_file in xml_files:
        # Get root
        file_path = os.path.join(ANN_DIR, xml_file)
        tree = ET.parse(file_path)
        root = tree.getroot()
        
        # Find elements
        filename_element = root.find('filename')
        path = root.find('path')

        if(os.path.exists(path.text)):
            print(re.sub('(\\.xml)|(\\.jpg)|(\\.jpeg)|(\\.png)', '', filename_element.text.lower()))
            correct += 1
        else:
            print('Verification Failed:', xml_file)
            if removefailed:
                os.remove(file_path)
        total += 1
    print(correct, 'out of', total, 'correct')

if(os.path.exists(ANN_DIR) == False):
    os.mkdir(ANN_DIR)

if(os.path.exists(IMG_DIR) == False):
    os.mkdir(IMG_DIR)

file_count = 0
xml_files = list(filter(lambda i: i.endswith('xml'), os.listdir(RAW_DIR)))
for xml_file in xml_files:
    change_path_value(xml_file)
    file_count += 1
print('Processed', file_count, 'files')

file_count = 0
img_files = list(filter(lambda i: i.lower().endswith('jpg') or i.lower().endswith('jpeg') or i.lower().endswith('png'), os.listdir(RAW_DIR)))
for img_file in img_files:
    rename_to_jpg(img_file)
    file_count += 1
print('Processed', file_count, 'files')

verify(True)