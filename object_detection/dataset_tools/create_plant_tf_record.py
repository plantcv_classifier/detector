import tensorflow as tf
import os
import xml.etree.ElementTree as ET

from object_detection.utils import dataset_util


flags = tf.app.flags
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
flags.DEFINE_string('input_path', '', 'Path to input directory containing JPEGImages and Annotations subdirs')
flags.DEFINE_boolean('eval', False, 'Generate evaluation records')

FLAGS = flags.FLAGS
IMAGE_DIR = 'JPEGImages'
ANNOTATION_DIR = 'Annotations'

CLASSES = ['guava', 'akapulko']

def normalize_bounds(bound_list, base):
  return list(map(lambda b: b / base, bound_list))

def create_tf_record(annotation_path):
  # TODO(user): Populate the following variables from your example.

  # read annotation
  tree = ET.parse(annotation_path)
  root = tree.getroot()

  size_element = root.find('size')
  height_text = size_element.find('height').text
  width_text = size_element.find('width').text

  object_element = root.find('object')
  class_name = object_element.find('name').text
  class_number = CLASSES.index(class_name) + 1

  bndbox_element = object_element.find('bndbox')
  xmin_text = bndbox_element.find('xmin').text
  ymin_text = bndbox_element.find('ymin').text
  xmax_text = bndbox_element.find('xmax').text
  ymax_text = bndbox_element.find('ymax').text

  filename_text = root.find('filename').text
  path = os.path.join(FLAGS.input_path, IMAGE_DIR, filename_text) #root.find('path').text
  with tf.gfile.GFile(path, 'rb') as fid:
    encoded_jpg = fid.read()

  # print('height_text:', height_text)
  # print('width_text:', width_text)
  # print('xmin_text:', xmin_text)
  # print('ymin_text:', ymin_text)
  # print('xmax_text:', xmax_text)
  # print('ymax_text:', ymax_text)
  # print('filename:', filename)
  # print('path:', path)
  # print('class_name:', class_name)
  # print('class_number:', class_number)

  height = int(height_text) # Image height
  width = int(width_text) # Image width
  filename = root.find('filename').text.encode('utf8') # Filename of the image. Empty if image is not from file
  encoded_image_data = encoded_jpg # Encoded image bytes
  image_format = 'jpeg'.encode('utf8') # b'jpeg' or b'png'

  xmins = [int(xmin_text)] # List of normalized left x coordinates in bounding box (1 per box)
  xmaxs = [int(xmax_text)] # List of normalized right x coordinates in bounding box
             # (1 per box)
  ymins = [int(ymin_text)] # List of normalized top y coordinates in bounding box (1 per box)
  ymaxs = [int(ymax_text)] # List of normalized bottom y coordinates in bounding box
             # (1 per box)
  classes_text = [class_name.encode('utf8')] # List of string class name of bounding box (1 per box)
  classes = [class_number] # List of integer class id of bounding box (1 per box)

  print(normalize_bounds(xmins, width))

  tf_record = tf.train.Example(features=tf.train.Features(feature={
      'image/height': dataset_util.int64_feature(height),
      'image/width': dataset_util.int64_feature(width),
      'image/filename': dataset_util.bytes_feature(filename),
      'image/source_id': dataset_util.bytes_feature(filename),
      'image/encoded': dataset_util.bytes_feature(encoded_image_data),
      'image/format': dataset_util.bytes_feature(image_format),
      'image/object/bbox/xmin': dataset_util.float_list_feature(normalize_bounds(xmins, width)),
      'image/object/bbox/xmax': dataset_util.float_list_feature(normalize_bounds(xmaxs, width)),
      'image/object/bbox/ymin': dataset_util.float_list_feature(normalize_bounds(ymins, height)),
      'image/object/bbox/ymax': dataset_util.float_list_feature(normalize_bounds(ymaxs, height)),
      'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
      'image/object/class/label': dataset_util.int64_list_feature(classes),
  }))
  return tf_record


def main(_):
  output_suffix = 'eval' if FLAGS.eval else 'train'
  output_file = 'data_' + output_suffix + '.record'
  writer = tf.python_io.TFRecordWriter(os.path.join(FLAGS.output_path, output_file))

  # TODO(user): Write code to read in your dataset to examples variable
  # print(FLAGS.input_path)
  # print(FLAGS.output_path)

  annotation_full_dir = os.path.join(FLAGS.input_path, ANNOTATION_DIR)
  annotation_list = os.listdir(annotation_full_dir)
  # annotation_list = ['10.xml', '11.xml', '89.xml', '$_20.xml', '77.xml']
  annotations = list(map(lambda f: os.path.join(annotation_full_dir, f), annotation_list))
  
  # print(annotations)

  for annotation in annotations:
    tf_record = create_tf_record(annotation)
    writer.write(tf_record.SerializeToString())

  # for example in examples:
  #   tf_example = create_tf_example(example)
    # writer.write(tf_example.SerializeToString())

  writer.close()
  print('Total records generated:', len(annotations))


if __name__ == '__main__':
  tf.app.run()