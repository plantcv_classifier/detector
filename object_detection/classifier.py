import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import plantcv as pcv
import os

from random import shuffle
from PIL import Image
from tensorflow.contrib import slim
from tensorflow.contrib.slim.python.slim.nets import resnet_v2

import matplotlib

CLASSES = ['guava', 'akapulko']

def plantcv_process(img, debug=False):
    device = 0
    device, s = pcv.rgb2gray_hsv(img, 's', device, debug)
    device, s_thresh = pcv.binary_threshold(s, 85, 255, 'light', device, debug)
    device, s_mblur = pcv.median_blur(s_thresh, 5, device, debug)
    device, s_cnt = pcv.median_blur(s_thresh, 5, device, debug)
    device, b = pcv.rgb2gray_lab(img, 'b', device, debug)
    device, b_thresh = pcv.binary_threshold(b, 160, 255, 'light', device, debug)
    device, b_cnt = pcv.binary_threshold(b, 160, 255, 'light', device, debug)
    device, bs = pcv.logical_or(s_mblur, b_cnt, device, debug)
    device, masked = pcv.apply_mask(img, bs, 'white', device, debug)
    device, masked_a = pcv.rgb2gray_lab(masked, 'a', device, debug)
    device, masked_b = pcv.rgb2gray_lab(masked, 'b', device, debug)
    device, maskeda_thresh = pcv.binary_threshold(masked_a, 115, 255, 'dark', device, debug)
    device, maskeda_thresh1 = pcv.binary_threshold(masked_a, 135, 255, 'light', device, debug)
    device, maskedb_thresh = pcv.binary_threshold(masked_b, 128, 255, 'light', device, debug)
    device, ab1 = pcv.logical_or(maskeda_thresh, maskedb_thresh, device, debug)
    device, ab = pcv.logical_or(maskeda_thresh1, ab1, device, debug)
    device, ab_cnt = pcv.logical_or(maskeda_thresh1, ab1, device, debug)
    device, ab_fill = pcv.fill(ab, ab_cnt, 200, device, debug)
    device, masked2 = pcv.apply_mask(masked, ab_fill, 'black', device, debug)
    return masked2

def resizing_process(raw_img, debug=False):
    base_size = 224

    img = Image.fromarray(np.uint8(raw_img))

    if img.size[0] != base_size or img.size[1] != base_size:
        if img.size[0] < img.size[1]:
            new_height = int(float(img.size[1]) * (base_size / float(img.size[0])))
            img = img.resize((base_size, new_height), Image.ANTIALIAS)
        else:
            new_width = int(float(img.size[0]) * (base_size / float(img.size[1])))
            img = img.resize((new_width, base_size), Image.ANTIALIAS)

    return img

def tensorflow_plantcv_process(img, debug=False):
    base_size = 224
    f = 'tempfile.jpg'
    if os.path.exists(f):
        os.remove(f)
    img.save(f)

    filename_queue = tf.train.string_input_producer([f])
    reader = tf.WholeFileReader()
    key, value = reader.read(filename_queue)
    
    decode_img = tf.image.decode_image(value, channels=3)
    decode_img = tf.image.resize_image_with_crop_or_pad(decode_img, base_size, base_size)

    init_op = tf.initialize_all_variables()
    with tf.Session() as sess:
        sess.run(init_op)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        image = decode_img.eval()
        image = plantcv_process(image, debug=debug)

        coord.request_stop()
        coord.join(threads)
    os.remove(f)
    return image

def preprocess_images(images, debug=False):
    processed_images = []

    for image in images:
        image = resizing_process(image)
        if debug == True:
            print('Resized Image')
            plt.imshow(image)
            plt.show()

        image = tensorflow_plantcv_process(image)
        if debug == True:
            print('PlantCV Processing')
            plt.imshow(image)
            plt.show()

        processed_images.append(image)

    return np.asarray(processed_images, dtype=np.float32)

def eval_images(images, model_dir):
    num_classes = len(CLASSES)
    with tf.Graph().as_default():
        tf.logging.set_verbosity(tf.logging.DEBUG)
        
        logits, end_points = resnet_v2.resnet_v2_50(images, num_classes=num_classes, is_training=False)
        logits = tf.reshape(logits, [len(images), num_classes])

        probabilities = tf.nn.softmax(logits)

        checkpoint_path = tf.train.latest_checkpoint(model_dir)
        init_fn = slim.assign_from_checkpoint_fn(checkpoint_path, slim.get_model_variables('resnet_v2_50'))

        sess = tf.Session()
        init_fn(sess)
        
        probs = sess.run(probabilities)

        return probs