import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import plantcv as pcv
import os

from random import shuffle
from PIL import Image
from tensorflow.contrib import slim
from tensorflow.contrib.slim.python.slim.nets import resnet_v2

# images should be stored on the directory of their label (ex: guava/1.jpg, akapulko/2.jpg)
MODEL_DIR = 'model'
TRAIN_DIR = 'training_images'
CLASSES = ['guava', 'akapulko']

BATCH_SIZE = 30
TOTAL_STEPS = 60000

# PlantCV preprocessing (http://plantcv.readthedocs.io/en/latest/vis_tutorial/)
def preprocess_image(img):
    device = 0
    debug = False
    device, s = pcv.rgb2gray_hsv(img, 's', device, debug)
    device, s_thresh = pcv.binary_threshold(s, 85, 255, 'light', device, debug)
    device, s_mblur = pcv.median_blur(s_thresh, 5, device, debug)
    device, s_cnt = pcv.median_blur(s_thresh, 5, device, debug)
    device, b = pcv.rgb2gray_lab(img, 'b', device, debug)
    device, b_thresh = pcv.binary_threshold(b, 160, 255, 'light', device, debug)
    device, b_cnt = pcv.binary_threshold(b, 160, 255, 'light', device, debug)
    #device, b_fill = pcv.fill(b_thresh, b_cnt, 10, device, debug)
    device, bs = pcv.logical_or(s_mblur, b_cnt, device, debug)
    device, masked = pcv.apply_mask(img, bs, 'white', device, debug)
    device, masked_a = pcv.rgb2gray_lab(masked, 'a', device, debug)
    device, masked_b = pcv.rgb2gray_lab(masked, 'b', device, debug)
    device, maskeda_thresh = pcv.binary_threshold(masked_a, 115, 255, 'dark', device, debug)
    device, maskeda_thresh1 = pcv.binary_threshold(masked_a, 135, 255, 'light', device, debug)
    device, maskedb_thresh = pcv.binary_threshold(masked_b, 128, 255, 'light', device, debug)
    device, ab1 = pcv.logical_or(maskeda_thresh, maskedb_thresh, device, debug)
    device, ab = pcv.logical_or(maskeda_thresh1, ab1, device, debug)
    device, ab_cnt = pcv.logical_or(maskeda_thresh1, ab1, device, debug)
    device, ab_fill = pcv.fill(ab, ab_cnt, 200, device, debug)
    device, masked2 = pcv.apply_mask(masked, ab_fill, 'black', device, debug)
    
    return masked2

def extract_input_details(directory):
    base_size = 224
    images, labels = [], []
    classes = os.listdir(directory)
    file_list = []

    # Create list of labels (per image)
    for class_item in classes:
        class_dir = os.path.join(directory, class_item)
        current_file_list = list(map(lambda f: os.path.join(class_dir, f), os.listdir(class_dir)))
        file_list = file_list + current_file_list
        label = CLASSES.index(class_item)
        labels = labels + [label for i in range(len(current_file_list))]

    # Shuffle images for training
    indices = [i for i in range(len(labels))]
    shuffle(indices)
    file_list = [file_list[i] for i in indices]
    labels = [labels[i] for i in indices]

    for i in range(len(file_list)):
        print(labels[i], file_list[i])

    # Resize all images to 224 x 224
    # Resized using Pillow and then cropped/padded using Tensorflow
    for file in file_list:
        img = Image.open(file)
        if img.size[0] != base_size or img.size[1] != base_size:
            if img.size[0] < img.size[1]:
                new_height = int(float(img.size[1]) * (base_size / float(img.size[0])))
                img = img.resize((base_size, new_height), Image.ANTIALIAS)
            else:
                new_width = int(float(img.size[0]) * (base_size / float(img.size[1])))
                img = img.resize((new_width, base_size), Image.ANTIALIAS)
            img.save(file)
    
    filename_queue = tf.train.string_input_producer(file_list)

    reader = tf.WholeFileReader()
    key, value = reader.read(filename_queue)
    
    # Setup retrieval of image data with 3 channels (RGB)
    decode_img = tf.image.decode_image(value, channels=3)
    decode_img = tf.image.resize_image_with_crop_or_pad(decode_img, 224, 224)

    # Actual getting of image data and adding it to the list
    init_op = tf.initialize_all_variables()
    with tf.Session() as sess:
        sess.run(init_op)

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        for i in range(len(file_list)):
            image = decode_img.eval()
            image = preprocess_image(image)
            images.append(image)

        coord.request_stop()
        coord.join(threads)

    # 1st list - image data, 2nd list - corresponding label
    return np.asarray(images, dtype=np.float32), labels

# Extract image details
all_images, all_labels = extract_input_details(TRAIN_DIR)

num_classes = len(CLASSES)

# Training proper
steps = 0
while steps < TOTAL_STEPS:
    index = 0
    # Run by batches to prevent overloading GPU
    while index < len(all_images):

        last_index = index + BATCH_SIZE if index + BATCH_SIZE <= len(all_images) else len(all_images)
        images = all_images[index:last_index]
        labels = all_labels[index:last_index]
        steps = steps + (last_index - index)

        print('Processing records:', len(images))
        with tf.Graph().as_default():
            tf.logging.set_verbosity(tf.logging.INFO)
        
            # Create the model:
            logits, end_points = resnet_v2.resnet_v2_50(images, num_classes=num_classes, is_training=True)
            logits = tf.reshape(logits, [last_index - index, num_classes])
            print('Logits:', logits)
                
            # Specify the loss function:
            one_hot_labels = slim.one_hot_encoding(labels, num_classes)
            slim.losses.softmax_cross_entropy(logits, one_hot_labels)
            total_loss = slim.losses.get_total_loss()

            # Create some summaries to visualize the training process:
            tf.summary.scalar('losses/Total_Loss', total_loss)
        
            # Specify the optimizer and create the train op:
            optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
            train_op = slim.learning.create_train_op(total_loss, optimizer)

            # Run the training:
            final_loss = slim.learning.train(
            train_op,
            logdir=MODEL_DIR,
            number_of_steps=steps,
            save_summaries_secs=60)
        
            print('Finished training. Final batch loss %d' % final_loss)

            event_files = list(filter(lambda f: f.startswith('events'), os.listdir(MODEL_DIR)))
            for event_file in event_files:
                os.remove(os.path.join(MODEL_DIR, event_file))

            index = index + BATCH_SIZE

            if steps >= TOTAL_STEPS:
                break